#!/usr/bin/env perl
# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
use Mojo::Base -base;
use Mojo::Collection 'c';
use Mojo::DOM;
use Mojo::URL;
use Mojo::UserAgent;
use Mojo::Util qw(encode decode);
use XML::RSS;

my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
   $year += 1900;
my $month = sprintf("%02d", ++$mon);
my $url   = 'https://mazette.media';

my $file  = $ENV{MAZETTE_RSSFILE};

die 'Please set MAZETTE_RSSFILE. Aborting.' unless $file;

my $ua   = Mojo::UserAgent->new();
my $feed = XML::RSS->new(version => '2.0');
$feed->channel(
    title       => 'Mazette ! Les actus',
    link        => $url,
    description => 'Les actus de Mazette du mois courant',
    language    => 'fr',
    generator   => 'mazette-rss'
);

# Get images
my $dom = Mojo::DOM->new($ua->get("$url/actu/")->result->body);
$dom->find('.actu-card')->each(sub {
    my ($e, $num) = @_;

    my $author = $e->find('.actu-card-head-left')->first->text;
    $author =~ s/^\s+|\s+$//m;
    my $img    = $e->find('.actu-card-body img')->first->attr('data-lazy-src');

    my %hash = (
        title       => $author,
        link        => $img,
        description => sprintf('<p>Par %s</p><div><img src="%s">', $author, $img),
        permaLink   => $img,
    );
    $feed->add_item(%hash);
});

$feed->save($file);
